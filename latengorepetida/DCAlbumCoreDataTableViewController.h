//
//  DCAlbumCoreDataTableViewController.h
//  latengorepetida
//
//  Created by David Céspedes on 29/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTCoreDataTableViewController.h"

@interface DCAlbumCoreDataTableViewController : AGTCoreDataTableViewController

@property (nonatomic, strong) NSString * team;

@end
