//
//  DCAppDelegate.h
//  latengorepetida
//
//  Created by David Céspedes on 29/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTCoreDataStack;

@interface DCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AGTCoreDataStack *model;

@end
