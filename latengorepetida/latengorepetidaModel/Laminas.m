#import "Laminas.h"


@interface Laminas ()

// Private interface goes here.

@end


@implementation Laminas

// Custom logic goes here.
+ (NSArray *)LaminasInManagedObjectContext:(NSManagedObjectContext *)moc {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Laminas entityName]];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"stickerNumber" ascending:YES]];
    
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"ERROR: %@ %@", [error localizedDescription], [error userInfo]);
        exit(1);
    }
    NSLog(@"The number of Laminas are: %d", [results count]);
    if ([results count] == 0) {
        return nil;
    }
    
    return results;
}

@end
