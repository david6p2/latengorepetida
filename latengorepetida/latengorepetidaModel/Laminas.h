#import "_Laminas.h"

@interface Laminas : _Laminas {}
// Custom logic goes here.
+ (NSArray *)LaminasInManagedObjectContext:(NSManagedObjectContext *)moc;
@end
