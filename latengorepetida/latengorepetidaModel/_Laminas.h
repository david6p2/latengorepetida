// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Laminas.h instead.

#import <CoreData/CoreData.h>


extern const struct LaminasAttributes {
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *repeated;
	__unsafe_unretained NSString *selected;
	__unsafe_unretained NSString *stickerImage;
	__unsafe_unretained NSString *stickerNumber;
	__unsafe_unretained NSString *team;
	__unsafe_unretained NSString *teamOrder;
} LaminasAttributes;

extern const struct LaminasRelationships {
	__unsafe_unretained NSString *user;
} LaminasRelationships;

extern const struct LaminasFetchedProperties {
} LaminasFetchedProperties;

@class User;









@interface LaminasID : NSManagedObjectID {}
@end

@interface _Laminas : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LaminasID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* repeated;



@property int16_t repeatedValue;
- (int16_t)repeatedValue;
- (void)setRepeatedValue:(int16_t)value_;

//- (BOOL)validateRepeated:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* selected;



@property BOOL selectedValue;
- (BOOL)selectedValue;
- (void)setSelectedValue:(BOOL)value_;

//- (BOOL)validateSelected:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* stickerImage;



//- (BOOL)validateStickerImage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* stickerNumber;



@property int16_t stickerNumberValue;
- (int16_t)stickerNumberValue;
- (void)setStickerNumberValue:(int16_t)value_;

//- (BOOL)validateStickerNumber:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* team;



//- (BOOL)validateTeam:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* teamOrder;



@property int16_t teamOrderValue;
- (int16_t)teamOrderValue;
- (void)setTeamOrderValue:(int16_t)value_;

//- (BOOL)validateTeamOrder:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) User *user;

//- (BOOL)validateUser:(id*)value_ error:(NSError**)error_;





@end

@interface _Laminas (CoreDataGeneratedAccessors)

@end

@interface _Laminas (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitiveRepeated;
- (void)setPrimitiveRepeated:(NSNumber*)value;

- (int16_t)primitiveRepeatedValue;
- (void)setPrimitiveRepeatedValue:(int16_t)value_;




- (NSNumber*)primitiveSelected;
- (void)setPrimitiveSelected:(NSNumber*)value;

- (BOOL)primitiveSelectedValue;
- (void)setPrimitiveSelectedValue:(BOOL)value_;




- (NSString*)primitiveStickerImage;
- (void)setPrimitiveStickerImage:(NSString*)value;




- (NSNumber*)primitiveStickerNumber;
- (void)setPrimitiveStickerNumber:(NSNumber*)value;

- (int16_t)primitiveStickerNumberValue;
- (void)setPrimitiveStickerNumberValue:(int16_t)value_;




- (NSString*)primitiveTeam;
- (void)setPrimitiveTeam:(NSString*)value;




- (NSNumber*)primitiveTeamOrder;
- (void)setPrimitiveTeamOrder:(NSNumber*)value;

- (int16_t)primitiveTeamOrderValue;
- (void)setPrimitiveTeamOrderValue:(int16_t)value_;





- (User*)primitiveUser;
- (void)setPrimitiveUser:(User*)value;


@end
