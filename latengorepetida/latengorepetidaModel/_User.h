// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.h instead.

#import <CoreData/CoreData.h>


extern const struct UserAttributes {
	__unsafe_unretained NSString *loginMethod;
	__unsafe_unretained NSString *userPicture;
	__unsafe_unretained NSString *username;
} UserAttributes;

extern const struct UserRelationships {
	__unsafe_unretained NSString *laminas;
} UserRelationships;

extern const struct UserFetchedProperties {
} UserFetchedProperties;

@class Laminas;





@interface UserID : NSManagedObjectID {}
@end

@interface _User : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (UserID*)objectID;





@property (nonatomic, strong) NSString* loginMethod;



//- (BOOL)validateLoginMethod:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* userPicture;



//- (BOOL)validateUserPicture:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* username;



//- (BOOL)validateUsername:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *laminas;

- (NSMutableSet*)laminasSet;





@end

@interface _User (CoreDataGeneratedAccessors)

- (void)addLaminas:(NSSet*)value_;
- (void)removeLaminas:(NSSet*)value_;
- (void)addLaminasObject:(Laminas*)value_;
- (void)removeLaminasObject:(Laminas*)value_;

@end

@interface _User (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveLoginMethod;
- (void)setPrimitiveLoginMethod:(NSString*)value;




- (NSString*)primitiveUserPicture;
- (void)setPrimitiveUserPicture:(NSString*)value;




- (NSString*)primitiveUsername;
- (void)setPrimitiveUsername:(NSString*)value;





- (NSMutableSet*)primitiveLaminas;
- (void)setPrimitiveLaminas:(NSMutableSet*)value;


@end
