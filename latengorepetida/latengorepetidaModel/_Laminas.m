// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Laminas.m instead.

#import "_Laminas.h"

const struct LaminasAttributes LaminasAttributes = {
	.name = @"name",
	.repeated = @"repeated",
	.selected = @"selected",
	.stickerImage = @"stickerImage",
	.stickerNumber = @"stickerNumber",
	.team = @"team",
	.teamOrder = @"teamOrder",
};

const struct LaminasRelationships LaminasRelationships = {
	.user = @"user",
};

const struct LaminasFetchedProperties LaminasFetchedProperties = {
};

@implementation LaminasID
@end

@implementation _Laminas

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Laminas" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Laminas";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Laminas" inManagedObjectContext:moc_];
}

- (LaminasID*)objectID {
	return (LaminasID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"repeatedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"repeated"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"selectedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"selected"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"stickerNumberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stickerNumber"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"teamOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"teamOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic name;






@dynamic repeated;



- (int16_t)repeatedValue {
	NSNumber *result = [self repeated];
	return [result shortValue];
}

- (void)setRepeatedValue:(int16_t)value_ {
	[self setRepeated:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveRepeatedValue {
	NSNumber *result = [self primitiveRepeated];
	return [result shortValue];
}

- (void)setPrimitiveRepeatedValue:(int16_t)value_ {
	[self setPrimitiveRepeated:[NSNumber numberWithShort:value_]];
}





@dynamic selected;



- (BOOL)selectedValue {
	NSNumber *result = [self selected];
	return [result boolValue];
}

- (void)setSelectedValue:(BOOL)value_ {
	[self setSelected:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSelectedValue {
	NSNumber *result = [self primitiveSelected];
	return [result boolValue];
}

- (void)setPrimitiveSelectedValue:(BOOL)value_ {
	[self setPrimitiveSelected:[NSNumber numberWithBool:value_]];
}





@dynamic stickerImage;






@dynamic stickerNumber;



- (int16_t)stickerNumberValue {
	NSNumber *result = [self stickerNumber];
	return [result shortValue];
}

- (void)setStickerNumberValue:(int16_t)value_ {
	[self setStickerNumber:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveStickerNumberValue {
	NSNumber *result = [self primitiveStickerNumber];
	return [result shortValue];
}

- (void)setPrimitiveStickerNumberValue:(int16_t)value_ {
	[self setPrimitiveStickerNumber:[NSNumber numberWithShort:value_]];
}





@dynamic team;






@dynamic teamOrder;



- (int16_t)teamOrderValue {
	NSNumber *result = [self teamOrder];
	return [result shortValue];
}

- (void)setTeamOrderValue:(int16_t)value_ {
	[self setTeamOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveTeamOrderValue {
	NSNumber *result = [self primitiveTeamOrder];
	return [result shortValue];
}

- (void)setPrimitiveTeamOrderValue:(int16_t)value_ {
	[self setPrimitiveTeamOrder:[NSNumber numberWithShort:value_]];
}





@dynamic user;

	






@end
