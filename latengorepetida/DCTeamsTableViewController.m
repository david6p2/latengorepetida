//
//  DCTeamsTableViewController.m
//  latengorepetida
//
//  Created by David Céspedes on 30/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import "DCTeamsTableViewController.h"
#import "DCAlbumCoreDataTableViewController.h"

@interface DCTeamsTableViewController ()

@end

@implementation DCTeamsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if (!self.teams) {
        self.teams = @[@"Especiales", @"Estadios", @"Brasil", @"Croacia", @"México", @"Camerún", @"España", @"Holanda", @"Chile", @"Australia", @"Colombia", @"Grecia", @"Costa de Marfíl", @"Japón", @"Uruguay", @"Costa Rica", @"Inglaterra", @"Italia", @"Suiza", @"Ecuador", @"Francia", @"Honduras", @"Argentina", @"Bosnia", @"Irán", @"Nigeria", @"Alemania", @"Portugal", @"Ghana", @"EE. UU.", @"Bélgica", @"Argelia", @"Rusia", @"Corea"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden{
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.teams.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    // Averiguar el notebook que nos piden
    NSString *equipo = [self.teams objectAtIndex:indexPath.row];
    
    // Crear un id de celda
    static NSString *cellID = @"TeamCell";
    
    //Pedirle a la tabla una celda que tenga a mano
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //Si no tiene ninguna crear una desde cero
    if (cell == nil) {
        //Crear de cero
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    
    //Configurarla: sincronizar celda (vista) con el equipo (modelo)
    cell.textLabel.text = equipo;
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    //La devolvemos
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"toAlbumTableVC"]) {
        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
        NSString *team = [self.teams objectAtIndex:path.row];
        NSLog(@"Going to show the team of (%@)", team);
        
        //Show Team
        DCAlbumCoreDataTableViewController * albumVC= (DCAlbumCoreDataTableViewController *)[segue destinationViewController];
        
        NSLog(@"DCAlbumCoreDataTableViewController is Coming From DCTeamsTableViewController");
        
        ///////////
        if (albumVC) {
            NSLog(@"The upcomingVC is %@",albumVC);
            albumVC.team = team;
            NSLog(@"The upcomingVC team is %@",albumVC.team);
        }else{
            NSLog(@"The upcomingVC is empty %@",albumVC);
        }

    }
}


@end
