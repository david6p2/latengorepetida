//
//  DCAppDelegate.m
//  latengorepetida
//
//  Created by David Céspedes on 29/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import "DCAppDelegate.h"

#import "AGTCoreDataStack.h"
#import "Laminas.h"
#import "User.h"

@implementation DCAppDelegate

- (void) dummyData{
    NSArray * laminas = [Laminas LaminasInManagedObjectContext:self.model.context];
    if (!laminas) {
        NSError* err = nil;
        NSString* dataPath = [[NSBundle mainBundle] pathForResource:@"laminas" ofType:@"json"];
        laminas = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:dataPath]
                                                  options:kNilOptions
                                                    error:&err];
        NSLog(@"Imported laminas: %@", laminas);
        User *user = [User insertInManagedObjectContext:self.model.context];
        user.username = @"david6p2";
        user.loginMethod = @"none";
        user.userPicture = @"";

        [self fillLaminasDataInCoreData:laminas forUser:user];
    }
}

- (void)fillLaminasDataInCoreData:(NSArray *)laminas forUser:(User *)user{
    for (NSDictionary * laminaDict in laminas) {
        Laminas * lamina = [Laminas insertInManagedObjectContext:self.model.context];
        //stickerNumber
        if (laminaDict[@"id"]) {
            lamina.stickerNumber = (NSNumber*)laminaDict[@"id"];
            lamina.stickerImage = [NSString stringWithFormat:@"http://www.latengorepetida.com/assets/images/stickers_mini/%@.jpg", lamina.stickerNumber];
        }else{
            lamina.stickerNumber = @1000;
            lamina.stickerImage = @"1000";
        }
        
        //name
        if (laminaDict[@"name"]) {
            lamina.name = laminaDict[@"name"];
        }else{
            lamina.name = @"";
        }
        
        //repeated
        if (laminaDict[@"repeated"]) {
            lamina.repeated = (NSNumber *)[laminaDict valueForKey:@"repeated"];
        }else{
            lamina.repeated = @0;
        }
        
        //selected
        if (laminaDict[@"selected"] != nil) {
            NSNumber * selectedValue = (NSNumber *)[laminaDict valueForKey:@"selected"];
            if ([selectedValue boolValue] == YES) {
                lamina.selected = [NSNumber numberWithBool:YES];
            }else{
                lamina.selected = [NSNumber numberWithBool:NO];
            }
            //lamina.selected = (NSNumber*)laminaDict[@"selected"];
        }else{
            lamina.selected = NO;
        }
        
        //team
        if (laminaDict[@"team"]) {
            lamina.team = laminaDict[@"team"];
        }else{
            lamina.team = @"NoTeam";
        }
        
        if (laminaDict[@"team"]) {
            if ([laminaDict[@"team"] isEqualToString:@"Argentina"]) {
                lamina.teamOrder = @0;
            }else if ([laminaDict[@"team"] isEqualToString:@"Colombia"]){
                lamina.teamOrder = @1;
            }else if ([laminaDict[@"team"] isEqualToString:@"Italia"]){
                lamina.teamOrder = @2;
            }
        }else{
            lamina.teamOrder = @1000;
        }
        
        lamina.user = user;
    }
    NSLog(@"Saving Laminas");
    [self.model saveWithErrorBlock:^(NSError *error) {
        NSLog(@"No se pudo crear datos de prueba");
    }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    //Inicializo el Modelo (CoreDataStack)
    self.model = [AGTCoreDataStack coreDataStackWithModelName:@"latengorepetidaModel"];
    
    //Probamos datos
    [self dummyData];
    
    //if (AUTO_SAVE) {
    //    [self autoSave];
    //}
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
