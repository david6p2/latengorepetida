//
//  DCSimpleSwipeTableViewCell.m
//  latengorepetida
//
//  Created by David Céspedes on 3/04/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import "DCSimpleSwipeTableViewCell.h"

//const CGFloat kRevealWidth = 40.0; //160.0
const CGFloat MaxDistance = 40;
static NSString *RevealCellDidOpenNotification = @"RevealCellDidOpenNotification";

@interface DCSimpleSwipeTableViewCell () <BSTapScrollViewDelegate> {
    BOOL _isOpen;
}


@property (nonatomic, strong) IBOutlet BSTapScrollView *scrollView;

@property (nonatomic, assign) BOOL hasChangedUnreadStatusRight;
@property (nonatomic, assign) BOOL hasChangedUnreadStatusLeft;
@property (nonatomic, assign) BOOL hasSwipeLeft;

@end

@implementation DCSimpleSwipeTableViewCell{
    
}

- (void)awakeFromNib{
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.tapDelegate = self;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onOpen:)
                                                 name:RevealCellDidOpenNotification
                                               object:nil];
}

- (void)onOpen:(NSNotification *)notification{
    if (notification.object != self) {
        if (_isOpen) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.25
                                 animations:^{
                                     self.scrollView.contentOffset = CGPointZero;
                                 }];
            });
        }
        
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    /*
    if (_scrollView == nil) {
        _scrollView = [[BSTapScrollView alloc] initWithFrame:self.contentView.bounds];
        _scrollView.contentSize = self.contentView.frame.size;
        _scrollView.alwaysBounceHorizontal = YES;
        _scrollView.delegate = self;
        self.scrollView.tapDelegate = self;
        
        [self.contentView insertSubview:_scrollView atIndex:0];
    }
    */
    
    //Borrar
    [self.scrollView addSubview:self.innerContentView];
    
    if (_unreadIndicatorRight == nil) {
        _unreadIndicatorRight = [[NSCRUnreadIndicator alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
        [self.contentView insertSubview:_unreadIndicatorRight
                           belowSubview:self.scrollView];
    }
    
    CGRect frameRight  = self.unreadIndicatorRight.frame;
    frameRight.origin.x = self.contentView.frame.size.width - self.unreadIndicatorRight.frame.size.width - 10;
    frameRight.origin.y = (self.contentView.frame.size.height - self.unreadIndicatorRight.frame.size.height)/2.0;
    self.unreadIndicatorRight.frame = frameRight;
    
    if (_unreadIndicatorLeft == nil) {
        _unreadIndicatorLeft = [[NSCRUnreadIndicator alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
        [self.contentView insertSubview:_unreadIndicatorLeft
                           belowSubview:self.scrollView];
    }
    
    CGRect frameLeft  = self.unreadIndicatorLeft.frame;
    frameLeft.origin.x = 10;
    frameLeft.origin.y = (self.contentView.frame.size.height - self.unreadIndicatorLeft.frame.size.height)/2.0;
    self.unreadIndicatorLeft.frame = frameLeft;
    
}

-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    [self setNeedsDisplay];
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //if (scrollView.contentOffset.x < 0) {
        //scrollView.contentOffset = CGPointZero;
    //}
    
    if (scrollView.contentOffset.x >= MaxDistance || scrollView.contentOffset.x <= -MaxDistance) {
        _isOpen = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:RevealCellDidOpenNotification object:self];
    }else{
        _isOpen = NO;
    }
    
    if (!self.hasChangedUnreadStatusRight) {
        if (scrollView.contentOffset.x > 0) {
            CGFloat progress = MIN(1,scrollView.contentOffset.x / MaxDistance);
            NSLog(@"%f",progress);
            [self.unreadIndicatorRight setFillPercent:progress];
            if (progress >=1) {
                self.unreadIndicatorRight.hasRead = !self.unreadIndicatorRight.hasRead;
                [self.unreadIndicatorRight setFillPercent:0];
                self.hasChangedUnreadStatusRight = YES;
            }
            
        }
    }
    if (!self.hasChangedUnreadStatusLeft){
        if (scrollView.contentOffset.x < 0){
            CGFloat progress = ABS(MAX(-1,scrollView.contentOffset.x / MaxDistance));
            NSLog(@"%f",progress);
            [self.unreadIndicatorLeft setFillPercent:progress];
            if (progress >=1) {
                self.unreadIndicatorLeft.hasRead = !self.unreadIndicatorLeft.hasRead;
                [self.unreadIndicatorLeft setFillPercent:0];
                self.hasChangedUnreadStatusLeft = YES;
            }
        }
    }
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:self];
    NSLog(@"/n/nIndexPath %@ for talbleView %@/n/n", indexPath, self.tableView);
    if (self.hasChangedUnreadStatusLeft) {
        [self.swipeDelegate swipeScrollViewToTheRight:self forIndexPath:indexPath];
        self.hasChangedUnreadStatusLeft = NO;
    }
    if (self.hasChangedUnreadStatusRight) {
        [self.swipeDelegate swipeScrollViewToTheLeft:self forIndexPath:indexPath];
        self.hasChangedUnreadStatusRight = NO;
    }
}

#pragma mark - BSTapScrollView Delegate

- (void)tapScrollView:(BSTapScrollView *)scrollView touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"Setting highlighted...");
    
    if (_isOpen) {
    } else {
        [self setHighlighted:YES animated:YES];
    }
}

- (void)tapScrollView:(BSTapScrollView *)scrollView touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"Setting normal...");
    [self setHighlighted:NO animated:NO];
}

- (void)tapScrollView:(BSTapScrollView *)scrollView touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_isOpen) {
        NSLog(@"_isOpen");
        return;
    }
    NSLog(@"did select....");
    NSIndexPath *indexPath = [self.tableView indexPathForCell:self];
    [self setHighlighted:NO animated:NO];
    [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RevealCellDidOpenNotification
                                                        object:self];
}
/*
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
*/


@end
