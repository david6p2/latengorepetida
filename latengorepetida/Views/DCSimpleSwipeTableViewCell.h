//
//  DCSimpleSwipeTableViewCell.h
//  latengorepetida
//
//  Created by David Céspedes on 3/04/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSCRUnreadIndicator.h"
#import "BSTapScrollView.h"


@protocol DCSimpleSwipeTableViewCellDelegate;

@interface DCSimpleSwipeTableViewCell : UITableViewCell <UIScrollViewDelegate>

@property (nonatomic, weak) id<DCSimpleSwipeTableViewCellDelegate> swipeDelegate;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIView *innerContentView;

@property (strong, nonatomic) IBOutlet UIImageView *cellImage;
@property (strong, nonatomic) IBOutlet UILabel *cellTitle;
@property (strong, nonatomic) IBOutlet UILabel *cellSubTitle;

@property(nonatomic, strong) NSCRUnreadIndicator * unreadIndicatorRight;
@property(nonatomic, strong) NSCRUnreadIndicator * unreadIndicatorLeft;

@end

@protocol DCSimpleSwipeTableViewCellDelegate <NSObject>

@optional

- (void)swipeScrollViewToTheLeft:(DCSimpleSwipeTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath;
- (void)swipeScrollViewToTheRight:(DCSimpleSwipeTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath;

@end
