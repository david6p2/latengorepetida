//
//  DCLaminaTableViewCell.h
//  latengorepetida
//
//  Created by David Céspedes on 30/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface DCLaminaTableViewCell : UITableViewCell <UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *innerContentView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *cellImage;
@property (strong, nonatomic) IBOutlet UILabel *cellTitle;
@property (strong, nonatomic) IBOutlet UILabel *cellSubTitle;

@end
