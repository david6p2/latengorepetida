//
//  DCViewController.m
//  latengorepetida
//
//  Created by David Céspedes on 29/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import "DCViewController.h"
#import "DCAlbumCoreDataTableViewController.h"

@interface DCViewController ()

@end

@implementation DCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
