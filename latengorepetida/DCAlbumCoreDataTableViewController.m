//
//  DCAlbumCoreDataTableViewController.m
//  latengorepetida
//
//  Created by David Céspedes on 29/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import "DCAlbumCoreDataTableViewController.h"

#import "DCAppDelegate.h"
#import "AGTCoreDataStack.h"
#import "Laminas.h"
#import "DCLaminaTableViewCell.h"
#import "DCSimpleSwipeTableViewCell.h"

#import <AFNetworking.h>

@interface DCAlbumCoreDataTableViewController () <DCSimpleSwipeTableViewCellDelegate>

@end

@implementation DCAlbumCoreDataTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.fetchedResultsController = [self setupFetchedResultsController];
        NSLog(@"Going to perform Fetch %@", self.fetchedResultsController.fetchRequest);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.title = self.team;
}

- (void)viewWillAppear:(BOOL)animated{
    if (!self.fetchedResultsController) {
        self.fetchedResultsController = [self setupFetchedResultsController];
        NSLog(@"Going to perform Fetch %@ in viewWillAppear", self.fetchedResultsController.fetchRequest);
    }
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}
*/
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Create Fetch Request
- (NSFetchedResultsController *)setupFetchedResultsController{
    
    //Traemos el App Delegate para acceder al modelo
    DCAppDelegate *app = (DCAppDelegate *)[[UIApplication sharedApplication] delegate];
    //Creamos un FetchRequest
    NSFetchRequest * allLaminas = [[NSFetchRequest alloc] initWithEntityName:[Laminas entityName]];
    allLaminas.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"stickerNumber" ascending:YES]];
    
    //Creamos el predicado para filtrar las notas
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self.team = %@", self.team];
    
    allLaminas.predicate = pred;
    
    //Creamos el FetchedResultsController
    NSFetchedResultsController *fetchedVC = [[NSFetchedResultsController alloc] initWithFetchRequest:allLaminas
                                                                                managedObjectContext:app.model.context
                                                                                  sectionNameKeyPath:nil
                                                                                           cacheName:nil];
    return fetchedVC;
}

#pragma mark - Table Data Source
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguar el notebook que nos piden
    Laminas *lamina = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Crear un id de celda
    static NSString *cellID = @"LaminaAlbumCell";
    
    DCSimpleSwipeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID
                                                                       forIndexPath:indexPath];
    cell.swipeDelegate = self;
    /*
    //Pedirle a la tabla una celda que tenga a mano
    DCLaminaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    //Si no tiene ninguna crear una desde cero
    if (cell == nil) {
        //Crear de cero
        cell = [[DCLaminaTableViewCell alloc] initWithStyle:0 reuseIdentifier:cellID];
    }
    */
    
    //Configurarla: sincronizar celda (vista) con notebook (modelo)
    cell.cellTitle.text = lamina.name;
    cell.cellSubTitle.text = lamina.team;
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    if (lamina.selectedValue) {
        UIColor *greenColor = [UIColor colorWithRed:85.0 / 255.0 green:213.0 / 255.0 blue:80.0 / 255.0 alpha:1.0];
        UIColor *yellowColor = [UIColor colorWithRed:254.0 / 255.0 green:217.0 / 255.0 blue:56.0 / 255.0 alpha:1.0];
        if (lamina.repeated.intValue > 0) {
            cell.innerContentView.backgroundColor = yellowColor;
            NSString * veces = @"Veces";
            if (lamina.repeated.intValue == 1) {
                veces = @"Vez";
            }
            cell.cellTitle.text = [NSString stringWithFormat:@"%@ - %d %@", cell.cellTitle.text, lamina.repeated.intValue, veces];
        }else{
            cell.innerContentView.backgroundColor = greenColor;
            
        }
    }
    //Establecer imagen de celda
    NSLog(@"To load %@ for indexPath.row %ld", lamina.stickerImage, (long)indexPath.row);
    //UIImageView *cellImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 128.0f, 128.0f)];
    //[view setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"Icon"]];
    
    [cell.cellImage setImageWithURL:[NSURL URLWithString:lamina.stickerImage]
                   placeholderImage:[UIImage imageNamed:@"1.png"]];
    /*
     [cellImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:lamina.stickerImage]]
     placeholderImage:[UIImage imageNamed:@"1.png"]
     success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
     cell.cellImage.image = image;
     NSLog(@"Loaded %@ for indexPath.row %ld", lamina.stickerImage, (long)indexPath.row);
     [tableView reloadRowsAtIndexPaths:@[indexPath]
     withRowAnimation:UITableViewRowAnimationAutomatic];
     //[self.tableView reloadData];
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
     
     }
     ];
     */
    //cell.imageView.image = cellImage.image;
    
    //La devolvemos
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        /*
        //[self.tableView beginUpdates];// Avoid  NSInternalInconsistencyException
        DCNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSLog(@"Deleting (%@)", nb.name);
        //[_objects removeObjectAtIndex:indexPath.row];
        [self.fetchedResultsController.managedObjectContext deleteObject:nb];
        NSError * error;
        if (![self.fetchedResultsController.managedObjectContext save:&error]) {
            NSLog(@"No se pudo borrar el Notebook por el error: %@", error);
        }
         */
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

#pragma mark - Table View Delegate
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Selected %@", indexPath);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    /*
     //Averiguar cual es la libreta
     DCNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
     
     //Creamos un FetchRequest
     NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:[DCNote entityName]];
     request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"modificationDate" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
     
     //Creamos el predicado para filtrar las notas
     NSPredicate *pred = [NSPredicate predicateWithFormat:@"self.notebook = %@", nb];
     
     request.predicate = pred;
     
     //Creamos el FetchedResultsController
     NSFetchedResultsController *fetchedVC = [[NSFetchedResultsController alloc] initWithFetchRequest:request
     managedObjectContext:self.fetchedResultsController.managedObjectContext
     sectionNameKeyPath:nil
     cacheName:nil];
     
     //Crear un Note VC
     DCNoteViewController * noteVC = [[DCNoteViewController alloc] initWithFetchedResultsController:fetchedVC
     style:UITableViewStylePlain
     noteBook:nb];
     
     //Hacer Push
     [self.navigationController pushViewController:noteVC
     animated:YES];
     */
    
}

#pragma mark - DCSimpleSwipeTableViewCellDelegate
- (void)swipeScrollViewToTheLeft:(DCSimpleSwipeTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath{
    if (indexPath) {
        NSLog(@"Cell in indexPath: %@ was scrolled to the Left", indexPath);
        Laminas *lamina = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSLog(@"Lamina # %@ was selected %@ and repeated %@ times", lamina.stickerNumber, lamina.selected, lamina.repeated);
        //If I got the sticker
        if (lamina.selectedValue) {
            //And it's repeated
            if (lamina.repeated.intValue > 0) {
                //take the number of times repeated
                int repeatedInt = lamina.repeated.intValue;
                //Minus 1 per eache left swipe
                repeatedInt--;
                //save it in the entity attribute
                lamina.repeated = [NSNumber numberWithInt:repeatedInt];
            }
            //is not repeated
            else if(lamina.repeated.intValue == 0) {
                //Make sure is not repeated
                lamina.repeated = @0;
                //set as not selected
                lamina.selectedValue = NO;
            }
        }
        NSLog(@"Lamina # %@ is now selected %@ and repeated %@ times", lamina.stickerNumber, lamina.selected, lamina.repeated);
    }else{
        NSLog(@"no indexPath for %@ even wen the tableViewSelectedRow is %@", cell, [self.tableView indexPathForSelectedRow]);
        NSLog(@"and indexpath for cell is %@", [self.tableView indexPathForCell:cell]);
    }
    
    
}
- (void)swipeScrollViewToTheRight:(DCSimpleSwipeTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath{
    if (indexPath) {
        NSLog(@"Cell in indexPath: %@ was scrolled to the Right", indexPath);
        Laminas *lamina = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSLog(@"Lamina # %@ was selected %@ and repeated %@ times", lamina.stickerNumber, lamina.selected, lamina.repeated);
        //If I got the sticker
        if (lamina.selectedValue) {
            //take the number of times repeated
            int repeatedInt = lamina.repeated.intValue;
            //Plus 1 per eache right swipe
            repeatedInt++;
            //save it in the entity attribute
            lamina.repeated = [NSNumber numberWithInt:repeatedInt];
        }else{
            lamina.selectedValue = YES;
        }
        NSLog(@"Lamina # %@ is now selected %@ and repeated %@ times", lamina.stickerNumber, lamina.selected, lamina.repeated);
    }else{
        NSLog(@"no indexPath for %@ even wen the tableViewSelectedRow is %@", cell, [self.tableView indexPathForSelectedRow]);
        NSLog(@"and indexpath for cell is %@", [self.tableView indexPathForCell:cell]);
    }
}

@end