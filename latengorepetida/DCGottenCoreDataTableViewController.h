//
//  DCGottenCoreDataTableViewController.h
//  latengorepetida
//
//  Created by David Céspedes on 30/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTCoreDataTableViewController.h"

@interface DCGottenCoreDataTableViewController : AGTCoreDataTableViewController

@end
