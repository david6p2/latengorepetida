//
//  main.m
//  latengorepetida
//
//  Created by David Céspedes on 29/03/14.
//  Copyright (c) 2014 LSR Marketing Service. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DCAppDelegate class]));
    }
}
